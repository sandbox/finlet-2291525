<?php

/**
 * @todo Write file documentation.
 */

/**
 * Implements hook_bulk_content_analysis_info().
 *
 * Use this hook to register you Bulk Content Analysis plugin.
 */
function hook_bulk_content_analysis_info() {
  //
}

/**
 * Implements hook_bulk_content_analyse().
 *
 * Bulk Content Analysis calls every implementation of this hook for every node
 * it has to analyse.
 */
function hook_bulk_content_analyse($node) {
  //
}
