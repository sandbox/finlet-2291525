<?php

/**
 * @todo Write file documentation.
 */

/**
 * @todo Write function documentation.
 */
function bulk_content_analysis_admin_settings($form, &$form_state) {
  $form = system_settings_form($form);

  $form['bulk_content_analysis_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Items per page'),
    '#description' => t('Set the number of items that are shown on one results page.'),
    '#default_value' => variable_get('bulk_content_analysis_limit', 25),
  );
  $form['bulk_content_analysis_hide_ok'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide nodes with status ok'),
    '#description' => t('If this box is checked all nodes that have status
      ok for all analyses are hidden.'),
    '#default_value' => variable_get('bulk_content_analysis_hide_ok', 0),
    '#return_value' => 1,
  );

  return $form;
}

/**
 * @todo Write function documentation.
 */
function bulk_content_analysis_admin_settings_submit($form, &$form_state) {
  $values = $form_state['values'];

  variable_set('bulk_content_analysis_limit', $values['bulk_content_analysis_limit']);
  variable_set('bulk_content_analysis_hide_ok', $values['bulk_content_analysis_hide_ok']);
}
