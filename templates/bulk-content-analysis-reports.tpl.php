<?php if (user_access('execute content analysis reports')): ?>
<p><?php print l(t('Start new analysis'), 'admin/reports/content-analysis',
        array('attributes' => array('class' => array('button')))); ?></p>
<?php endif; ?>
<?php if (!empty($tables)): ?>
  <?php foreach ($tables as $table): ?>
    <?php print $table; ?>
  <?php endforeach; ?>
<?php else: ?>
<p><?php print t('There are no analysis results to display'); ?>.</p>
<?php endif; ?>
<?php if (!empty($pager)): ?>
  <?php print $pager; ?>
<?php endif; ?>