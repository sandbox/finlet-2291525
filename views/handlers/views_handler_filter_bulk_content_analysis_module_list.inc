<?php

/**
 * @todo Write file documentation.
 */

/**
 * @todo Write class documentation.
 */
class views_handler_filter_bulk_content_analysis_module_list extends views_handler_filter_many_to_one {
  /**
   * @todo Write function documentation.
   */
  function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);

    $default_value = (array) $this->value;

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];

      if (!empty($this->options['expose']['reduce'])) {
        $options = $this->reduce_value_options($options);

        if (!empty($this->options['expose']['multiple']) && empty($this->options['expose']['required'])) {
          $default_value = array();
        }
      }

      if (empty($this->options['expose']['multiple'])) {
        if (empty($this->options['expose']['required']) && (empty($default_value) || !empty($this->options['expose']['reduce']))) {
          $default_value = 'All';
        }
        elseif (empty($default_value)) {
          $keys = array_keys($options);
          $default_value = array_shift($keys);
        }
        // Due to #1464174 there is a chance that array('') was saved in the admin ui.
        // Let's choose a safe default value.
        elseif ($default_value == array('')) {
          $default_value = 'All';
        }
        else {
          $copy = $default_value;
          $default_value = array_shift($copy);
        }
      }
    }

    $form['value']['#type'] = 'select';
    $form['value']['#multiple'] = TRUE;
    $form['value']['#size'] = 4;

    $info = bulk_content_analysis_get_all_analysis_info();
    $options = array();
    foreach ($info as $module => $module_info) {
      $options[$module] = $module_info['name'];
    }
    $form['value']['#options'] = $options;
    $form['value']['#default_value'] = $default_value;
  }
}
