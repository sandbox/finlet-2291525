<?php

/**
 * @todo Write file documentation.
 */

/**
 * @todo Write class documentation.
 */
class views_handler_field_bulk_content_analysis_module_list extends views_handler_field {
  /**
   * @todo Write function documentation.
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['display_format'] = array('default' => 'human_name');

    return $options;
  }

  /**
   * @todo Write function documentation.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['display_format'] = array(
      '#title' => t('Display format'),
      '#type' => 'select',
      '#options' => array(
        'machine_name' => t('Machine name'),
        'human_name' => t('Human readable name'),
      ),
      '#default_value' => $this->options['display_format'],
    );
  }

  /**
   * @todo Write function documentation.
   */
  function render($values) {
    $module = $values->{$this->field_alias};

    if ($this->options['display_format'] == 'machine_name') {
      return $module;
    } else {
      $info = bulk_content_analysis_get_analysis_info($module);

      return $info['name'];
    }
  }
}
