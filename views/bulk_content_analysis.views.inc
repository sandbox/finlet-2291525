<?php

/**
 * @todo Write file documentation.
 */

/**
 * Implements hook_views_data().
 */
function bulk_content_analysis_views_data() {
  return array(
    'bulk_content_analysis_results' => array(
      'table' => array(
        'group' => 'Bulk Content Analysis Results',
        'base' => array(
          'field' => 'nid',
          'title' => t('Bulk Content Analysis Results'),
          'help' => t('Table that contains the results of analysis with
            Bulk Content Analysis plugins') . '.',
          'weight' => -15,
        ),
        'join' => array(
          'node' => array(
            'left_field' => 'nid',
            'field' => 'nid',
          ),
        ),
      ),
      'nid' => array(
        'title' => t('Node'),
        'help' => t('The analysed node') . '.',
        'relationship' => array(
          'base' => 'node',
          'base field' => 'nid',
          'handler' => 'views_handler_relationship',
          'label' => t('Analysed node'),
          'title' => t('The node that has been analysed'),
        ),
      ),
      'module' => array(
        'title' => t('Module'),
        'help' => t('The module implementing the analysis') . '.',
        'field' => array(
          'handler' => 'views_handler_field_bulk_content_analysis_module_list',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_bulk_content_analysis_module_list',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      'status' => array(
        'title' => t('Status'),
        'help' => t('The status of the last analysis of this node') . '.',
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_bulk_content_analysis_status_list',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_string',
        ),
      ),
      'message' => array(
        'title' => t('Message'),
        'help' => t('The result of the last analysis of this node') . '.',
        'field' => array(
          'handler' => 'views_handler_field',
          'click sortable' => FALSE,
        ),
      ),
      'timestamp' => array(
        'title' => t('Last analysis'),
        'help' => t('The timestamp of the last analysis for this node
          by this module') . '.',
        'field' => array(
          'handler' => 'views_handler_field_date',
          'click sortable' => TRUE,
        ),
        'sort' => array(
          'handler' => 'views_handler_sort_date',
        ),
        'filter' => array(
          'handler' => 'views_handler_filter_date',
        ),
        'argument' => array(
          'handler' => 'views_handler_argument_date',
        ),
      ),
    ),
  );
}

/**
 * Implements of hook_views_handlers().
 */
function bulk_content_analysis_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'bulk_content_analysis')
        . '/views/handlers',
    ),
    'handlers' => array(
      'views_handler_field_bulk_content_analysis_module_list' => array(
        'parent' => 'views_handler_field',
      ),
      'views_handler_filter_bulk_content_analysis_module_list' => array(
        'parent' => 'views_handler_filter_many_to_one',
      ),
      'views_handler_filter_bulk_content_analysis_status_list' => array(
        'parent' => 'views_handler_filter_many_to_one',
      ),
    ),
  );
}

/**
 * Implements hook_views_pre_render().
 */
function bulk_content_analysis_views_pre_render(&$view) {
  if ($view->name == 'content_analysis') {
    drupal_add_css(drupal_get_path('module', 'bulk_content_analysis') . '/css/report.css');
  }
}

/**
 * Implements hook_views_post_render().
 */
function bulk_content_analysis_views_post_render(&$view, &$output, &$cache) {
  /** @todo Move to template? */
  if ($view->name == 'content_analysis') {
    $output = str_replace('class="views-table', 'class="views-table bulk-content-analysis system-status-report', $output);
  }
}
